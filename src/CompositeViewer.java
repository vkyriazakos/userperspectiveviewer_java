import java.awt.Component;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

public class CompositeViewer extends Component {

	private static final long serialVersionUID = 2326108903843403040L;
	//How many Mask frames to filter
	final int MASK_FILTER_FRAMES=3;
	//Array to store previous Masks
	private int mImageMasks[][];
	//Array to output filtered Mask , values 0 to MASK_FILTER_FRAMES
	private int mImageROI[];
	//Index showing current position in circular buffer
	private int ImageMasksIndex;
	int[] mImagePixels,mImageRestPixels; //Two arrays
	BufferedImage mBufferedImage;
	DepthViewer mDepth;
	RGBViewer mRGB;
	UserPerspectiveViewer mUserPerspectiveViewer;
	float mHistogram[];
	

	public CompositeViewer(RGBViewer rgbViewer, DepthViewer depthViewer) {
		mRGB = rgbViewer;
		mDepth = depthViewer;
		this.setSize(mDepth.getWidth(), mDepth.getHeight());
		// this.setSize(640,480);
		mImagePixels = new int[getWidth() * getHeight()];
		mImageRestPixels = new int[getWidth() * getHeight()];
		mImageMasks = new int[MASK_FILTER_FRAMES][Constant.DEPTH_WIDTH*Constant.DEPTH_HEIGHT];
		mImageROI = new int[Constant.DEPTH_WIDTH*Constant.DEPTH_HEIGHT];
		ImageMasksIndex=0;
		//out.print("Comp: " + this.getWidth() + "x" + this.getHeight() + "\n");
	}

	public void readyToPaint() {
		//*
		if (mImagePixels == null
				|| mImagePixels.length < mDepth.mLastFrame.getWidth()
						* mDepth.mLastFrame.getHeight()) {
			mImagePixels = new int[mDepth.mLastFrame.getWidth()
					* mDepth.mLastFrame.getHeight()];
			mImageRestPixels = new int[mDepth.mLastFrame.getWidth() 
                    * mDepth.mLastFrame.getHeight()];
		}
		// Existing Data
		int depth, red, green, blue;
		// Composite Data
		//int cred, cgreen, cblue;
		//First Scanline Mask to Fix errors
		int gap=0,preGapElement=0;
		final int GAP_THRESHOLD=50;
		boolean foundGap=false;
		//*
		for (int i = 0; i < mDepth.mLastFrame.getWidth() * mDepth.mLastFrame.getHeight(); i++) {
			//Chech if pre-gap depth and post-gap depth match in ROI, else leave it be
			depth = mDepth.mImageDepthPixels[i];
			//if ((i%100)==0) out.println(depth);
			if(foundGap==false){
				//if pregap=ROI area and gap found
				if(preGapElement == 1 && depth==0){
					foundGap=true;
					//out.println("Gap Found");
					gap++;
				}
				//else check if pregap=ROI area
				else if(depth<mDepth.ROI && depth!=0){
					preGapElement=1;
				}
			//else if on gap
			}else if(foundGap==true){
				//If still on gap just calculate gap pixels
				if(depth==0){
					gap++;
				}
				//If gap ended
				if(depth!=0){
					if(depth<mDepth.ROI && gap <=GAP_THRESHOLD){
						for(int j=i;j>=i-gap;j--){
							mDepth.mImageROIMask[j]=1;
						}
						//out.println("Gap Corrected");
					}
					//Gap ended anyway, so
					//Reset settings
					foundGap=false;
					gap=0;
					preGapElement=0;
				}
			}
		}
		//Now put ROI Mask to Array and Calculate Filtered ROI Mask
		for (int i = 0; i < mDepth.mLastFrame.getWidth() * mDepth.mLastFrame.getHeight(); i++) {
			mImageROI[i]=0;
			//Put new ROI Mask to Arrays
			mImageMasks[ImageMasksIndex][i]=mDepth.mImageROIMask[i];
			//Add Up Arrays
			for(int j=0;j<MASK_FILTER_FRAMES;j++){
				mImageROI[i]+=mImageMasks[j][i];
			}
		}
		//Set new image mask index
		ImageMasksIndex++;
		if(ImageMasksIndex==MASK_FILTER_FRAMES) ImageMasksIndex=0;
		//
		// setSize(mDepth.getWidth(),mDepth.getHeight());
		//Construct Composite Image
		//out.print("Comp: " + this.getWidth() + "x" + this.getHeight() + "\n");
		for (int i = 0; i < mDepth.mLastFrame.getWidth() * mDepth.mLastFrame.getHeight(); i++) {
			// Get DepthPixel Data and multiply RGB Data with it
			red = (mRGB.mImagePixels[i] & 0x00FF0000) >> 16;
			red &= 0xFF;
			green = (mRGB.mImagePixels[i] & 0x0000FF00) >> 8;
			green &= 0xFF;
			blue = (mRGB.mImagePixels[i] & 0x000000FF);
			blue &= 0xFF;
			//depth = (mDepth.mImagePixels[i] & 0x0000FF00) >> 8;
			//depth = mImageROI[i]*0xFF;
			depth = mImageROI[i]*Math.floorDiv(255, MASK_FILTER_FRAMES);
			depth &= 0xFF;
			//cred = ((red * depth) >> 8) & 0xFF;
			//cgreen = ((green * depth) >> 8) & 0xFF;
			//cblue = ((blue * depth) >> 8) & 0xFF;

			// mImagePixels[i] = 0xFF000000 | (cred << 16) | (cgreen << 8) |
			// cblue;
			mImagePixels[i] = (depth << 24) | (red << 16) | (green << 8) | blue;
			mImageRestPixels[i] = ((255-depth) << 24) | (red << 16) | (green << 8) | blue;
		
		}
		//*/
		repaint();
		mUserPerspectiveViewer.readyToPaint();
	}
	
	public void setUserPerspectiveViewer(UserPerspectiveViewer userPerspectiveViewer){
		mUserPerspectiveViewer=userPerspectiveViewer;
	}
	
	@Override
	public synchronized void paint(Graphics g) {
		int width=mDepth.getWidth(),height=mDepth.getHeight();
		if (mDepth.mLastFrame != null) {
			width = mDepth.mLastFrame.getWidth();
			height = mDepth.mLastFrame.getHeight();
		}
		if(width!=640||height!=480){
			width=640;height=480;
		}
		
		// make sure we have enough room
		if (mBufferedImage == null || mBufferedImage.getWidth() != width
				|| mBufferedImage.getHeight() != height) {
			mBufferedImage = new BufferedImage(width, height,
					BufferedImage.TYPE_INT_ARGB);
		}

		mBufferedImage.setRGB(0, 0, width, height, mImagePixels, 0, width);

		int framePosX = (getWidth() - width) / 2;
		int framePosY = (getHeight() - height) / 2;
		g.drawImage(mBufferedImage, framePosX, framePosY, null);
	}

}
