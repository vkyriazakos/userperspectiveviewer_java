import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

import javax.swing.JFrame;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfRect;
import org.opencv.core.Point;
import org.opencv.core.Point3;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.*;
import org.opencv.imgproc.*;
import org.opencv.objdetect.CascadeClassifier;
import org.opencv.objdetect.Objdetect;
//import org.opencv.core.Core;  


public class EyeTracker extends JFrame implements Runnable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -5472760685063533007L;
	//Constants
	//final int cameraID=0;
	final double pupilDistance=54.5;
	final double cFOV=Math.toRadians(46.4);
	final double cFOV_tan=Math.tan(cFOV/2.0);
	final double cameraOffsetX=0;
	final double cameraOffsetY=155; //Monitor Height Half
	final double cameraOffsetZ=20;
	final int bufferFrames = 10;
	
	//members
	private Point3 eye;
	private Point lEye,rEye;
	private CircularBuffer<Double> xBuffer,yBuffer,zBuffer;
	public EyeGrabber mEyeGrabber;
	
	private Thread t;
	private String threadName;
	public boolean mShouldRun;
	//VideoCapture mVideoCapture;
	CascadeClassifier mFaceClassifier,mLeftEyeClassifier,mRightEyeClassifier;
	MatOfRect mFaceDetections,mLeftEyeDetections,mRightEyeDetections;
	
	private Mat mImage,mGrayImage,mLeftEyeImage,mRightEyeImage;
	BufferedImage mBufImg;
	//Boogie class
	
	//Constructors
	public EyeTracker(){
		super("Eye Tracker");
		eye=new Point3();
		threadName = "Eye Tracker Thread";
		mImage = new Mat();
		mShouldRun=true;
		mEyeGrabber = new EyeGrabber();
		xBuffer = new CircularBuffer<>(bufferFrames);
		yBuffer = new CircularBuffer<>(bufferFrames);
		zBuffer = new CircularBuffer<>(bufferFrames);
		
		setLayout(new BorderLayout());
		setSize(640,480);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	public EyeTracker(double x,double y,double z){
		super("Eye Tracker");
		eye=new Point3(x,y,z);
		threadName = "Eye Tracker Thread";
		mImage = new Mat();
		mShouldRun=true;
		mEyeGrabber = new EyeGrabber();
		xBuffer = new CircularBuffer<>(bufferFrames);
		yBuffer = new CircularBuffer<>(bufferFrames);
		zBuffer = new CircularBuffer<>(bufferFrames);
		
		setLayout(new BorderLayout());
		setSize(640,480);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
	}
	
	//Getter
	public synchronized Point3 getEye(){
		return eye;
	}
	
	//Setters
	public synchronized void setEyeOffsetX(double X){
		eye.x=X;
	}
	public synchronized void setEyeOffsetY(double Y){
		eye.y=Y;
	}
	public synchronized void setEyeDepth(double Z){
		eye.z=Z;
	}
	//runnable and start method for multi-threading
	@Override
	public void run() {
		mFaceClassifier = new CascadeClassifier("data/haarcascade_frontalface_alt.xml");
		mLeftEyeClassifier = new CascadeClassifier("data/haarcascade_lefteye_2splits.xml");
		mRightEyeClassifier = new CascadeClassifier("data/haarcascade_righteye_2splits.xml");
		mFaceDetections = new MatOfRect();
		mLeftEyeDetections = new MatOfRect();
		mRightEyeDetections = new MatOfRect();
		mGrayImage = new Mat();
		
		//Check if Camera is Opened
	//	if(mEyeGrabber.mVideoCapture.isOpened()){
			while(mShouldRun){
				//Initialize 2D Eyes to NULL
				lEye=null;
				rEye=null;
				mEyeGrabber.getLastFrame(mImage);
				//System.out.println(mImage.type());
				Imgproc.cvtColor(mImage, mGrayImage, Imgproc.COLOR_BGR2GRAY);
				//mGrayImage=mImage;
				
				//Trace Face
				mFaceClassifier.detectMultiScale(mGrayImage,mFaceDetections,1.1,2,2,new Size(0,0),new Size());
				//For each face Find Eyes
				for(Rect faceRect : mFaceDetections.toArray()){
					Core.rectangle(mImage, new Point(faceRect.x,faceRect.y), new Point(faceRect.x+faceRect.width,faceRect.y+faceRect.height)	
					, new Scalar(0.0,255,0.0), 1);
					//Set Eye Area ROIs and Crop image
					Rect rightEyeROI = new Rect(faceRect.x +faceRect.width/16,(int)(faceRect.y + (faceRect.height/6)),
							(faceRect.width - 2*faceRect.width/16)/2,(int)( faceRect.height/2.5));
					Rect leftEyeROI = new Rect(faceRect.x +faceRect.width/16 +(faceRect.width - 2*faceRect.width/16)/2,
							(int)(faceRect.y + (faceRect.height/6)),(faceRect.width - 2*faceRect.width/16)/2,(int)( faceRect.height/2.5));
					//rightEyeROI = faceRect;
					//leftEyeROI = faceRect;
					rightEyeROI = new Rect((int)faceRect.x,(int)faceRect.y,(int)(faceRect.width/2.0),(int)faceRect.height);
					leftEyeROI = new Rect((int)faceRect.x+(int)(faceRect.width/2.0),(int)faceRect.y,(int)(faceRect.width/2.0),(int)faceRect.height);
					mLeftEyeImage = mGrayImage.submat(leftEyeROI);
					mRightEyeImage = mGrayImage.submat(rightEyeROI);
					//Also Draw them
					Core.rectangle(mImage, new Point(rightEyeROI.x,rightEyeROI.y), new Point(rightEyeROI.x+rightEyeROI.width,rightEyeROI.y+rightEyeROI.height)	
					, new Scalar(255,255,255), 1);
					Core.rectangle(mImage, new Point(leftEyeROI.x,leftEyeROI.y), new Point(leftEyeROI.x+leftEyeROI.width,leftEyeROI.y+leftEyeROI.height)	
					, new Scalar(255,255,255), 1);
					//Find Eyes
					
					//Find Left EYE
					mLeftEyeClassifier.detectMultiScale(mLeftEyeImage, mLeftEyeDetections,1.15, 2,Objdetect.CASCADE_FIND_BIGGEST_OBJECT|Objdetect.CASCADE_SCALE_IMAGE, new Size(0,0),new Size());
					//if (mLeftEyeDetections.toArray().length==0) System.out.println("Empty Detection");
					for(Rect eyeRect : mLeftEyeDetections.toArray()){
						Core.rectangle(mImage.submat(leftEyeROI), new Point(eyeRect.x,eyeRect.y), 
								new Point(eyeRect.x+eyeRect.width,eyeRect.y+eyeRect.height)	
						, new Scalar(0.0,0.0,255), 2);
						Core.rectangle(mImage.submat(leftEyeROI), new Point(eyeRect.x+eyeRect.width/3,eyeRect.y+eyeRect.height/2), 
								new Point(eyeRect.x+2*eyeRect.width/3,eyeRect.y+4*eyeRect.height/5)	
						, new Scalar(255,255,255), 1);
						//left Eye Definition w.r.t Initial Image
						lEye = new Point((double)leftEyeROI.x+(double)eyeRect.x+(double)eyeRect.width/2.0,(double)leftEyeROI.y+(double)eyeRect.y+13.0*(double)eyeRect.height/20.0);
						Core.circle(mImage,lEye,2,new Scalar(255,255,0,255),2);
						//Core.MinMaxLocResult mmG = Core.minMaxLoc(mLeftEyeImage.submat(eyeRect));
						//Core.circle(mImage.submat(leftEyeROI).submat(eyeRect), mmG.minLoc,2, new Scalar(255, 255, 255, 255),2);
						//lEye=mmG.minLoc.clone();
					}				
					//Find Right EYE
					mRightEyeClassifier.detectMultiScale(mRightEyeImage, mRightEyeDetections,1.15, 2,Objdetect.CASCADE_FIND_BIGGEST_OBJECT|Objdetect.CASCADE_SCALE_IMAGE, new Size(0,0),new Size());
					for(Rect eyeRect : mRightEyeDetections.toArray()){
						Core.rectangle(mImage.submat(rightEyeROI), new Point(eyeRect.x,eyeRect.y), 
								new Point(eyeRect.x+eyeRect.width,eyeRect.y+eyeRect.height)	
						, new Scalar(0.0,0.0,255), 2);
						Core.rectangle(mImage.submat(rightEyeROI), new Point(eyeRect.x+eyeRect.width/3,eyeRect.y+eyeRect.height/2), 
								new Point(eyeRect.x+2*eyeRect.width/3,eyeRect.y+4*eyeRect.height/5)	
						, new Scalar(255,255,255), 1);
						//Right Eye Definition w.r.t Initial Image
						rEye = new Point((double)rightEyeROI.x+(double)eyeRect.x+(double)eyeRect.width/2,(double)rightEyeROI.y+(double)eyeRect.y+13.0*(double)eyeRect.height/20.0);
						Core.circle(mImage,rEye,2,new Scalar(255,255,255,255),2);
						//Core.MinMaxLocResult mmG = Core.minMaxLoc(mRightEyeImage.submat(eyeRect));
						//Core.circle(mImage.submat(rightEyeROI).submat(eyeRect), mmG.minLoc,2, new Scalar(255, 255, 255, 255),2);
						//rEye=mmG.minLoc.clone();
					}
				}
				setEye();
				//Convert to BufferedImage and Paint
				//TODO CHANGED CAPTURE TO GRAY
				mBufImg=matToBuffered(mImage);
				
				repaint();
			}
		//}
		//Exiting
		mEyeGrabber.stop();
	}
	
	private void setEye(){
		//Change only if Point Found
		if(lEye!=null && rEye!=null){
			//Calculate eye pixel distance
			double pixelDistance=Math.sqrt((lEye.x-rEye.x)*(lEye.x-rEye.x)+(lEye.y-rEye.y)*(lEye.y-rEye.y));
			//EstimateDepth
			double depth=((pupilDistance*mGrayImage.cols())/(2*pixelDistance*cFOV_tan));
			depth +=cameraOffsetZ;
			
			//Find w.r.t image center
			double centerX=(double)mGrayImage.cols()/2.0;
			double centerY=(double)mGrayImage.rows()/2.0;
			//Estimate X
			double x=-((((lEye.x+rEye.x-2*centerX)/2)*pupilDistance)/pixelDistance);
			x+=cameraOffsetX;
			
			//Estimate Y
			double y=-((((lEye.y+rEye.y-2*centerY)/2)*pupilDistance)/pixelDistance);
			y+=cameraOffsetY;
			yBuffer.Add(y); xBuffer.Add(x); zBuffer.Add(depth);
			
			eye.x=xBuffer.getAverage();eye.y=yBuffer.getAverage();eye.z=zBuffer.getAverage();
		}
	}
	public void start (){
      System.out.println("Starting " +  threadName );
      mEyeGrabber.start();
      try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
      setVisible(true);
      if (t == null)
      {
         t = new Thread (this, threadName);
         t.start ();
      }
   }
	
	
	@Override
	public synchronized void paint (Graphics g){
		if(mBufImg!=null){
			int width=mBufImg.getWidth();
			int height=mBufImg.getHeight();
			
			int framePosX = (getWidth() - width) / 2;
			int framePosY = (getHeight() - height) / 2;
			g.drawImage(mBufImg, framePosX, framePosY, null);
		}
	}
	static public Mat loadImage(BufferedImage myBufferedImage){
		//Put Buffered Image into matrix to Process
		BufferedImage image = myBufferedImage;
		byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		Mat mat = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC3);
		mat.put(0, 0, data);
		return mat;
	}
	
	static public BufferedImage matToBuffered(Mat mat) {
		//Put Matrix into Buffered Image to Draw in Graphics
		byte[] data = new byte[mat.rows()*mat.cols()*(int)(mat.elemSize())];
		mat.get(0, 0, data);
		if (mat.channels() == 3) {
			for (int i = 0; i < data.length; i += 3) {
				byte temp = data[i];
				data[i] = data[i + 2];
				data[i + 2] = temp;
			}
		}
		BufferedImage image = new BufferedImage(mat.cols(), mat.rows(), BufferedImage.TYPE_3BYTE_BGR);
		image.getRaster().setDataElements(0, 0, mat.cols(), mat.rows(), data);
		return image;
	}
	
}
/////////////////////////////////////////////////////////////////////////////
//
// Create class to grab images from camera so as to not fill Picture buffer
//
//////////////////////////////////////////////////////////////////////////////
class EyeGrabber implements Runnable{

	private Mat lastFrame;
	private VideoCapture mVideoCapture;
	final int cameraID=0;
	String threadName;
	Thread t;
	private boolean mShouldRun;
	
	public EyeGrabber(){
		lastFrame = new Mat();
		threadName = "Camera Grabber";
		mShouldRun = true;
	}
	@Override
	public void run() {
		Mat temp = new Mat();
		if(mVideoCapture.isOpened()){
			while(mShouldRun){
				
			mVideoCapture.read(temp);
			lastFrame = temp;
			//Imgproc.cvtColor(temp, lastFrame, Imgproc.COLOR_BGR2GRAY);
			}
		}
		mVideoCapture.release();		
	}
	public void start(){
		System.out.println("Starting Camera Grabber");
		mVideoCapture = new VideoCapture(cameraID);
		try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (t == null)
	      {
	         t = new Thread (this, threadName);
	         t.start ();
	      }
	}
	
	public void stop(){
		mShouldRun = false;
		
	}
	
	public synchronized void getLastFrame(Mat dest){
		//System.out.println(lastFrame.type());
		lastFrame.copyTo(dest);
	}
}
