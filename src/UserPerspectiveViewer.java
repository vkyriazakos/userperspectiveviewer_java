import java.awt.BorderLayout;
import java.awt.Graphics;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;

import javax.swing.JFrame;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.highgui.Highgui;
import org.opencv.imgproc.Imgproc;

import com.sun.javafx.geom.Vec4f;
//import org.opencv.core.Core;  
import static java.lang.System.out;

public class UserPerspectiveViewer extends JFrame {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -635868888062372237L;
	//DEFINITIONS
	final public double PI=3.1415;
	final public double hFOV=57;
	final public double hFOV_rad= Math.toRadians(hFOV);
	final public double hFOV_tan=(Math.tan(hFOV_rad/2.0));
	final public int vFOV=43;
	final public double vFOV_rad=(Math.toRadians(vFOV));
	final public double vFOV_tan=(Math.tan(vFOV_rad/2.0));
	final public int monitorWidth=447;
	final public int monitorHeight=268;
	final public int xOffset=9;
	final public int yOffset=17;
	final public int zOffset=85;
	final int depthBufferFrames = 4;
	//Test Definitions
	final int videoWidth=640;
	final int videoHeight=480;
	private EyeTracker mEye;
	private EyeDrawer mEyeDrawer;
	//How much to change Eye Position on key stroke 
	final double keyPosDiff=10.0;
	
	//Members
	public boolean visible=false;;
	Mat backImage,userPerspectiveImage,croppedImage;
	BufferedImage mBufferedImage;
	CompositeViewer mCompositeViewer;
	CircularBuffer<Double> depthBufferROI,depthBufferBehind;

	
	public UserPerspectiveViewer(CompositeViewer compositeViewer){
		
		super("User Perspective Viewer");
		setLayout(new BorderLayout());
		
		//Get Ref to Composite Viewer
		mCompositeViewer=compositeViewer;
		//create OpenCV Compliant Buffered Image
		mBufferedImage = new BufferedImage(videoWidth, videoHeight,
				BufferedImage.TYPE_INT_ARGB);
		//OpenCv Matrices
		backImage= new Mat(videoHeight, videoWidth, CvType.CV_8UC4);
		userPerspectiveImage = new Mat();
		croppedImage = new Mat();
		
		//start Eye Tracker (For now Key Controlled)
		mEye = new EyeTracker(0, 0, 1000);
		mEyeDrawer = new EyeDrawer(mEye);
		
		//Initiate Depth Buffer	
		depthBufferROI = new CircularBuffer<>(depthBufferFrames);	
		depthBufferBehind = new CircularBuffer<>(depthBufferFrames);
		
		//KeyListeners
		addKeyListener(new KeyAdapter() {

	         @Override
	         public void keyTyped(KeyEvent e) {
	            myKeyEvt(e, "keyTyped");
	         }

	         @Override
	         public void keyReleased(KeyEvent e) {
	            myKeyEvt(e, "keyReleased");
	         }

	         @Override
	         public void keyPressed(KeyEvent e) {
	            myKeyEvt(e, "keyPressed");
	         }

	         private void myKeyEvt(KeyEvent e, String text) {
	            int key = e.getKeyCode();
	           // System.out.println("TEST");
	            
	            if ( (key == KeyEvent.VK_KP_LEFT || key == KeyEvent.VK_LEFT)&&(text=="keyPressed") )
	            {
	             //   System.out.println(text + " LEFT");
	            	mEye.setEyeOffsetX(mEye.getEye().x-keyPosDiff);
	                // mEye.eye.x-=keyPosDiff;
	            }
	            else if ( (key == KeyEvent.VK_KP_RIGHT || key == KeyEvent.VK_RIGHT)&&(text=="keyPressed") )
	            {
	             //   System.out.println(text + " RIGHT");
	            	mEye.setEyeOffsetX(mEye.getEye().x+keyPosDiff);
	            	//mEye.eye.x+=keyPosDiff;
	            }
	            else if ( (key == KeyEvent.VK_KP_UP || key == KeyEvent.VK_UP)&&(text=="keyPressed") )
	            {
	             //   System.out.println(text + " UP");
	            	mEye.setEyeOffsetY(mEye.getEye().y+keyPosDiff);
	            	//mEye.eye.y+=keyPosDiff;
	            }
	            else if ( (key == KeyEvent.VK_KP_DOWN || key == KeyEvent.VK_DOWN)&&(text=="keyPressed") )
	            {
	             //   System.out.println(text + " DOWN");
	            	mEye.setEyeOffsetY(mEye.getEye().y-keyPosDiff);
	            	//mEye.eye.y-=keyPosDiff;
	            }
	            else if ( (key == KeyEvent.VK_PAGE_UP )&&(text=="keyPressed") )
	            {
	             //   System.out.println(text + " PLUS");
	            	mEye.setEyeDepth(mEye.getEye().z+keyPosDiff);
	                //mEye.eye.z+=keyPosDiff;
	            }
	            else if ( (key == KeyEvent.VK_PAGE_DOWN )&&(text=="keyPressed") )
	            {
	             //   System.out.println(text + " MINUS");
	            	mEye.setEyeDepth(mEye.getEye().z-keyPosDiff);
	            	//mEye.eye.z-=keyPosDiff;
	            }
	         }
		});
	}
	
	@Override
	public void setVisible(boolean b){
		super.setVisible(b)	;
		visible=true;
		mEyeDrawer.setVisible(b);
		mEyeDrawer.setAlwaysOnTop(true);
		mEye.start();
	}
	
	@Override
	public void dispose(){
		super.dispose();
		mEyeDrawer.dispose();
		mEye.mShouldRun=false;
		mEye.dispose();
	}
	static public Mat loadImage(BufferedImage myBufferedImage){
		//Put Buffered Image into matrix to Process
		BufferedImage image = myBufferedImage;
		byte[] data = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
		Mat mat = new Mat(image.getHeight(), image.getWidth(), CvType.CV_8UC4);
		mat.put(0, 0, data);
		return mat;
	}
	
	static public BufferedImage matToBuffered(Mat mat) {
		//Put Matrix into Buffered Image to Draw in Graphics
		byte[] data = new byte[mat.rows()*mat.cols()*(int)(mat.elemSize())];
		mat.get(0, 0, data);
		if (mat.channels() == 4) {
			for (int i = 0; i < data.length; i += 4) {
				byte temp = data[i];
				data[i] = data[i + 3];
				data[i + 3] = temp;
				temp = data[i+1];
				data[i+1] = data[i + 2];
				data[i + 2] = temp;
			}
		}
		BufferedImage image = new BufferedImage(mat.cols(), mat.rows(), BufferedImage.TYPE_4BYTE_ABGR);
		image.getRaster().setDataElements(0, 0, mat.cols(), mat.rows(), data);
		return image;
	}
	


	public void readyToPaint() {
		if(visible){
			//Create two mat for ROi and behind
			Mat ROI = new Mat();
			Mat Behind = new Mat();
			//Simple RGB Test
			BufferedImage mybuff = new BufferedImage(videoWidth, videoHeight, BufferedImage.TYPE_4BYTE_ABGR);
			//First image
			mybuff.setRGB(0,0,videoWidth, videoHeight, mCompositeViewer.mImagePixels,0,videoWidth);
			backImage= new Mat(videoHeight, videoWidth, CvType.CV_8UC4);
			//backImage=Mat.zeros(videoHeight, videoWidth, CvType.CV_8UC4);
			backImage=loadImage(mybuff);
			
			Mat temp = new Mat(backImage.rows(),backImage.cols(),CvType.CV_8UC4);
						
			Core.flip(backImage,temp,1);
			
			backImage=temp;
			//Add perspective Rectangle
			depthBufferROI.Add((double)mCompositeViewer.mDepth.mHistogramViewer.getROI());
			double ROIdepth = depthBufferROI.getAverage() ;
			perspectiveImage(ROIdepth,ROI);
			
			//Second Image
			//mybuff.setRGB(0,0,videoWidth, videoHeight, mCompositeViewer.mImageRestPixels,0,videoWidth);
			mybuff.setRGB(0,0,videoWidth, videoHeight, mCompositeViewer.mRGB.mImagePixels,0,videoWidth);
			backImage= new Mat(videoHeight, videoWidth, CvType.CV_8UC4);
			//backImage=Mat.zeros(videoHeight, videoWidth, CvType.CV_8UC4);
			backImage=loadImage(mybuff);
			
			temp = new Mat(backImage.rows(),backImage.cols(),CvType.CV_8UC4);
						
			Core.flip(backImage,temp,1);
			
			backImage=temp;
			//Add perspective Rectangle
			depthBufferBehind.Add((double)(mCompositeViewer.mDepth.mHistogramViewer.maxDepth+mCompositeViewer.mDepth.mHistogramViewer.avgDepth)/2.0);
			double OtherDepth = depthBufferBehind.getAverage() ;
			perspectiveImage(OtherDepth,Behind);
			//out.println("Image Loaded to Matrix");
			//mBufferedImage=matToBuffered(userPerspectiveImage);
			
			//Generate Added Image
			//userPerspectiveImage = new Mat(new Size(1920,1080),CvType.CV_8UC4);
			//overlayImages(Behind,ROI,userPerspectiveImage);
			mBufferedImage=matToBuffered(Behind);
			//mBufferedImage=addMatsToBuffered(Behind,ROI);
			repaint();
			//Also Draw Eye position
			mEyeDrawer.repaint();
		}
		
	}

	static public BufferedImage addMatsToBuffered(Mat mat1,Mat mat2) {
		//Put Matrix into Buffered Image to Draw in Graphics
		short depth1, depth2, blue1, blue2, green1, green2, red1, red2;
		byte[] data1 = new byte[mat1.rows()*mat1.cols()*(int)(mat1.elemSize())];
		mat1.get(0, 0, data1);
		
		byte[] data2 = new byte[mat2.rows()*mat2.cols()*(int)(mat2.elemSize())];
		mat2.get(0, 0, data2);
		
		//Start with first mat in it
		byte[] data = new byte[mat1.rows()*mat1.cols()*(int)(mat1.elemSize())];
		mat1.get(0, 0, data);
		//ABGR to RGBA
		for (int i = 0; i < data.length; i += 4) {
			//ABGR now
			depth2=(short)(data2[i]<<1&0xFF);
			
			
			//if(depth2 > 0){
			//data[i] = (byte)(((data2[i]*depth2 + data[i]*(255-depth2) )>>8) & 0xFF);
			data[i]=data1[i];
			data[i + 1] = data1[i+1];
			
			data[i+2] = data1[i+2];
			data[i + 3] = data1[i+3];
			
			//data[i]=(byte) 0;
			//data[i + 1] = 0;
			
			//data[i+2] = 0;
			//data[i + 3] = 0;
			//}
			if(depth2>0) {
				data[i]=data2[i];
				data[i + 1] = data2[i+1];
				
				data[i+2] = data2[i+2];
				data[i + 3] = data2[i+3];
			}
			//ABGR to RGBA
			byte temp=data[i];
			data[i]=data[i+3];
			data[i+3]=temp;
			temp = data[i+2];
			data[i+2]=data[i+1];
			data[i+1]=temp;
		}
		
		BufferedImage image = new BufferedImage(mat1.cols(), mat1.rows(), BufferedImage.TYPE_4BYTE_ABGR);
		image.getRaster().setDataElements(0, 0, mat1.cols(), mat1.rows(), data);
		return image;
	}
	@Override
	public synchronized void paint(Graphics g) {
		int width=mBufferedImage.getWidth();
		int height=mBufferedImage.getHeight();
		
		int framePosX = (getWidth() - width) / 2;
		int framePosY = (getHeight() - height) / 2;
		g.clearRect(0, 0, getWidth(), getHeight());
		g.drawImage(mBufferedImage, framePosX, framePosY, null);
	}
	
	private void perspectiveImage(double Depth,Mat mPerspective){
		
		//Test with avg Depth for now
		
		double uniDepth =Depth+ (double)zOffset;
		
		//Get Eye depth from EyeTracker
		double eyeDistance = mEye.getEye().z;
		
		final float screenRatio = (float)(16.0/9.0);
		//final float cameraRatio= (float)(4.0/3.0);
		
		//Perspective view Center image(Initial Center 0,0)
		int centerX0 = backImage.cols()/2;
		int centerY0 = backImage.rows()/2;
		
		//Calculate Center Due to Offset
		int centerX = (int) (centerX0-(xOffset*backImage.cols())/(2*uniDepth*hFOV_tan));
		int centerY = (int) (centerY0+(yOffset*backImage.rows())/(2*uniDepth*vFOV_tan));
		
		//Calculate Image Width and Height
		double num= (double) monitorWidth *(double)(eyeDistance+uniDepth)*(double)backImage.cols();
		//out.println(perspectiveWidth);	
		double den = (double)(2*eyeDistance*uniDepth*hFOV_tan)  ;
		//out.println(den);
		int perspectiveWidth =(int) Math.round(num/den);
		int perspectiveHeight =(int)(perspectiveWidth/screenRatio);
		
		//Saturate Width, Height
		//perspectiveWidth = Math.min(perspectiveWidth, backImage.cols());
		//perspectiveHeight = Math.min(perspectiveHeight,(int)(backImage.cols()/screenRatio));
		
		
		
		//Calculate Center offset due to Eye Position
		centerX-=(int) ((mEye.getEye().x*backImage.cols())/(2*eyeDistance*hFOV_tan));
		centerY+=(int) ((mEye.getEye().y*backImage.rows())/(2*eyeDistance*vFOV_tan));
		Rect cropArea = new Rect();
		Rect cropOriginal = new Rect();
		//Find Top Left Translation and Bottom Right Translation 
		Point topLeftDiff = new Point(-centerX+(int)(perspectiveWidth/2 +0.5),-centerY+(int)(perspectiveHeight/2+0.5));
		Point bottomRightDiff = new Point();
		Point bottomRight = new Point(backImage.cols(),backImage.rows()) ;
		Point bottomRight_new = new Point( centerX+(int)(perspectiveWidth/2+0.5), centerY + (int)(perspectiveHeight/2+0.5));
		bottomRightDiff.x = bottomRight.x - bottomRight_new.x ;
		bottomRightDiff.y = bottomRight.y - bottomRight_new.y ;
		//bottomRightDiff.x = backImage.cols() - topLeftDiff.x - perspectiveWidth ;
		//bottomRightDiff.y = backImage.rows() - topLeftDiff.y - perspectiveHeight;
		
		//Check if both top left and bottom right are inside Image
		//out.println("tl neg: " + (isNegative(topLeftDiff)) + " br pos: " + (isPositive(bottomRightDiff)));
		if(isNegative(topLeftDiff) && isPositive(bottomRightDiff)){
			
			cropArea.x = (int)-topLeftDiff.x ;
			cropArea.y = (int)-topLeftDiff.y ;
			cropArea.height = perspectiveHeight;
			cropArea.width = perspectiveWidth;
			
			croppedImage=backImage.submat(cropArea);
		}
		else{
			
			Mat temp = backImage.clone();
			backImage = Mat.zeros(perspectiveHeight, perspectiveWidth, CvType.CV_8UC4);
			
			//Crop Original Image
			cropOriginal.x = intSaturate((int) -topLeftDiff.x, 0, temp.cols());
			cropOriginal.y = intSaturate((int) -topLeftDiff.y, 0, temp.rows());
			cropOriginal.width = intSaturate(temp.cols()-(int)bottomRightDiff.x - cropOriginal.x, 0, temp.cols() - cropOriginal.x);
			cropOriginal.height = intSaturate(temp.rows()-(int)bottomRightDiff.y - cropOriginal.y, 0, temp.rows() - cropOriginal.y);
			
			cropArea.x = intSaturate((int) topLeftDiff.x,0,perspectiveWidth);
			cropArea.y = intSaturate((int) topLeftDiff.y,0,perspectiveHeight);
			cropArea.width = intSaturate((int) (perspectiveWidth + bottomRightDiff.x - cropArea.x ),0,perspectiveWidth - cropArea.x) ;
			cropArea.height = intSaturate((int) (perspectiveHeight + bottomRightDiff.y - cropArea.y ),0,perspectiveHeight - cropArea.y);
			
			//If Sizes Differ 1 pixel, Keep Smaller
			if(Math.abs(cropArea.width-cropOriginal.width)==1 || Math.abs(cropArea.height-cropOriginal.height)==1){
			cropArea.width=Math.min(cropArea.width, cropOriginal.width);
			cropOriginal.width=cropArea.width;
			cropArea.height=Math.min(cropArea.height, cropOriginal.height);
			cropOriginal.height=cropArea.height;
			}else { 
				//out.println("Not One!");
				//out.println("Orig Cropped Size: "+cropOriginal.width+" x "+cropOriginal.height+" Add Area Size: "+cropArea.width+" x "+ cropArea.height);
			
			}
			//TODO: One Image Done : Bring COmposite Image and Overlay
			//out.println("Orig Cropped Size: "+cropOriginal.width+" x "+cropOriginal.height+" Add Area Size: "+cropArea.width+" x "+ cropArea.height);
			temp = temp.submat(cropOriginal);
			Mat addArea = new Mat();
			addArea = backImage.submat(cropArea);
			//out.println("Orig Cropped Size: "+temp.cols()+" x "+temp.rows()+" Add Area Size: "+addArea.cols()+" x "+ addArea.rows());
			//out.println("Type: "+temp.type()+" "+addArea.type());
			Core.addWeighted(temp, 1, addArea, 0, 0, addArea);
			croppedImage = backImage;
		}
		/*
		//Check if Desired Size is Larger than Actual camera Screen
		if(perspectiveWidth <= backImage.cols() && perspectiveHeight <= (int)(backImage.cols()/screenRatio)){
			
			cropArea.x = centerX-perspectiveWidth/2;
			cropArea.y = centerY-perspectiveHeight/2;
			cropArea.height = perspectiveHeight;
			cropArea.width = perspectiveWidth;
			
			//Saturate Crop Area to not exceed Camera Limits
			cropArea.x = Math.max(cropArea.x, 0);
			cropArea.y = Math.max(cropArea.y, 0);
			//And for Exceeding
			if(cropArea.x+cropArea.width>=backImage.cols()){
				cropArea.x=backImage.cols()-cropArea.width;
			}
			if(cropArea.y+cropArea.height>=backImage.rows()){
				cropArea.y=backImage.rows()-cropArea.height;
			}	
		}
		else {//Image Larger
			out.println("Width: " + perspectiveWidth);
			out.println("Height: " + perspectiveHeight);
			//Mat test = new Mat(perspectiveWidth,perspectiveHeight,CvType.CV_8UC3);
			Mat temp = backImage.clone();
		
			backImage = Mat.zeros(perspectiveHeight, perspectiveWidth, CvType.CV_8UC3);
			
			cropArea.x = centerX - temp.cols()/2 + perspectiveWidth/2 ;
			cropArea.y = centerY - temp.rows()/2 + perspectiveHeight/2 ;
			cropArea.width = temp.cols();
			cropArea.height = temp.rows();
			
			//Saturate Crop Area to not exceed Camera Limits
			cropArea.x = Math.max(cropArea.x, 0);
			cropArea.y = Math.max(cropArea.y, 0);
			//And for Exceeding
			if(cropArea.x+cropArea.width>=backImage.cols()){
				cropArea.width=backImage.cols()-cropArea.x;
			}
			if(cropArea.y+cropArea.height>=backImage.rows()){
				cropArea.height=backImage.rows()-cropArea.y;
				
			}
			//Fix original image so to keep Used Part
			cropOriginal.x = 0;
			cropOriginal.y = 0;
			cropOriginal.width = cropArea.width;
			cropOriginal.height = cropArea.height;
			out.println("Width Crop Original: " + cropOriginal.width);
			out.println("Height Crop Original: " + cropOriginal.height);
			temp = temp.submat(cropOriginal);
			
			Mat addArea = new Mat();
			out.println("X: " + cropArea.x);
			out.println("Y: " + cropArea.y);
			out.println("Xend: " + (cropArea.x+cropArea.width));
			out.println("Yend: " + (cropArea.y+cropArea.height));
			addArea = backImage.submat(cropArea);
			Core.addWeighted(addArea, 0, temp, 1, 0, addArea);
		}
		//Draw Square for Crop Area for Debugging
		/*
		Point ptAA=new Point(cropArea.x,cropArea.y);
		Point ptBB=new Point(cropArea.x+cropArea.width, cropArea.y+cropArea.height);
		Scalar color = new Scalar(1, 255, 1);
		Core.rectangle(backImage,ptAA,ptBB,color,3,8,0);
		*/
		
		//out.format("cols %d rows %d width %d height %d tlx %d tly %d brx %d bry %d \n",backImage.cols(),backImage.rows()
		//		,(int)cropArea.width,(int)cropArea.height,(int)cropArea.tl().x,(int)cropArea.tl().y,
		//		(int)cropArea.br().x,(int)cropArea.br().y);
		//out.format("width x height = %d x %d \n",cropArea.width,cropArea.height);
		//Proper Draw
		//*
		
		
		Size size = new Size(1920,1080);
		//userPerspectiveImage = Mat.zeros(size, CvType.CV_8UC4);
		Imgproc.resize(croppedImage, mPerspective, size);
		
	}	
	//Vector in 3rd Quarter (Both Negative)
	static boolean isNegative(Point a){
		
		if (a.x < 0 && a.y < 0) {
			return true;
		}
		return false;		
	}
	
	//Vector in 1st Quarter (Both Positive)
	static boolean isPositive(Point a){
		
		if (a.x > 0 && a.y > 0) {
			return true;
		}
		return false;		
	}
	
	static int intSaturate(int number, int min, int max){
		if(number<min){
			return min;
		}
		if(number>max){
			return max;
		}
		return number;
	}



}