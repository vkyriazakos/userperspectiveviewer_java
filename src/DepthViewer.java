import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.openni.VideoMode;
import org.openni.VideoStream;
//import static java.lang.System.out;

public class DepthViewer extends AbstractViewer{

	private static final long serialVersionUID = 3L;
	public int mImageROIMask[];
	public int mImageDepthPixels[];
	public HistogramViewer mHistogramViewer;
	private CompositeViewer mCompositeViewer;
	public int ROI;
	
	public DepthViewer(VideoStream videostream,HistogramViewer histogramViewer) {
		super(videostream);
		//Reference Histogram Viewer 
		mHistogramViewer=histogramViewer;
		
		//Setup Depth Stream		
		this.videoStream.setVideoMode(new VideoMode(
				Constant.DEPTH_WIDTH, 
				Constant.DEPTH_HEIGHT,
				Constant.DEPTH_FPS,
				Constant.DEPTH_PIXEL_FORMAT));
		//videoStream.setMirroringEnabled(false);
		setSize(Constant.DEPTH_WIDTH, Constant.DEPTH_HEIGHT);
	}
	
	
	@Override
	public synchronized void onFrameReady(VideoStream arg0) {

		if (mLastFrame != null) {
            mLastFrame.release();
            mLastFrame = null;
        }
        
        mLastFrame = videoStream.readFrame();
        ByteBuffer frameData = mLastFrame.getData().order(ByteOrder.LITTLE_ENDIAN);
        
        // make sure we have enough room
        if (mImagePixels == null || mImagePixels.length < mLastFrame.getWidth() * mLastFrame.getHeight()) {
            mImagePixels = new int[mLastFrame.getWidth() * mLastFrame.getHeight()];
        }
        mImageROIMask = new int[mLastFrame.getWidth() * mLastFrame.getHeight()];
        mImageDepthPixels = new int[mLastFrame.getWidth() * mLastFrame.getHeight()];
        //out.print("Depth: "+mLastFrame.getWidth()+"x"+mLastFrame.getHeight()+"\n");
        
    	calcHist(frameData);
    	//Get ROI layer
    	ROI=mHistogramViewer.getROI();
    	//out.println(ROI);
        frameData.rewind();
        int pos = 0;
        while(frameData.remaining() > 0) {
            int depth = (int)frameData.getShort() & 0xFFFF;
            //Depth Array
            mImageDepthPixels[pos]=depth;
            short pixel = (short)mHistogram[depth];
            mImagePixels[pos] = 0xFF000000 | (pixel << 16) | (pixel << 8);
            //Calculate Mask also
            if(depth<=ROI && pixel != 0){
            	 mImageROIMask[pos]=1;
            }else{
            	mImageROIMask[pos]=0;}  
            pos++;
        }
        //repaint();
        //Test call Composite Viewer to paint
        mCompositeViewer.readyToPaint();
	}
	
	
	public void setCompositeViewer(CompositeViewer compositeViewer){
		mCompositeViewer=compositeViewer;
	}
	
	private void calcHist(ByteBuffer depthBuffer) {
		
		//ROI to mask Video
		
        // make sure we have enough room
        if (mHistogram == null || mHistogram.length < videoStream.getMaxPixelValue()) {
            mHistogram = new float[videoStream.getMaxPixelValue()];
        }
        //out.println(videoStream.getMaxPixelValue());
        // reset
        for (int i = 0; i < mHistogram.length; ++i)
            mHistogram[i] = 0;
        
        //Make Normal histogram
        int points = 0;
        while (depthBuffer.remaining() > 0) {
            int depth = depthBuffer.getShort() & 0xFFFF;
            if (depth != 0) {
                mHistogram[depth]++;
                points++;
            }
        }
        mHistogramViewer.fillAndDraw(mHistogram);
        //And then make Summing Histogram to achieve coloring properties
        for (int i = 1; i < mHistogram.length; i++) {
        
            mHistogram[i] += mHistogram[i - 1];
        }
        if (points > 0) {
            for (int i = 1; i < mHistogram.length; i++) {
                mHistogram[i] = (int) (256 * (1.0f - (mHistogram[i] / (float) points)));
            }
        }
	}
}
