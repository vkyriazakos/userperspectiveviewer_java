import java.awt.Component;
import java.awt.Graphics;
import java.awt.image.BufferedImage;

import org.openni.Device;
import org.openni.VideoFrameRef;
import org.openni.VideoStream;

public abstract class AbstractViewer extends Component 
								implements VideoStream.NewFrameListener {
	
private static final long serialVersionUID = 1L;
	
    float mHistogram[];
    public int[] mImagePixels;
    public VideoStream videoStream;
    public Device mDevice;
    public VideoFrameRef mLastFrame;
    public BufferedImage mBufferedImage;

    public AbstractViewer(VideoStream videostream){
    	//Basic Constructor
    	this.videoStream=videostream;
    	
		videoStream.addNewFrameListener(this);
    }
        
    @Override
    abstract public void onFrameReady(VideoStream arg0);

    @Override
    public synchronized void paint(Graphics g) {
        if (mLastFrame == null) {
            return;
        }
        
        int width = mLastFrame.getWidth();
        int height = mLastFrame.getHeight();

        // make sure we have enough room
        if (mBufferedImage == null || mBufferedImage.getWidth() != width || mBufferedImage.getHeight() != height) {
            mBufferedImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        }
        
        mBufferedImage.setRGB(0, 0, width, height, mImagePixels, 0, width);
      
        int framePosX = (getWidth() - width) / 2;
        int framePosY = (getHeight() - height) / 2;
        g.drawImage(mBufferedImage, framePosX, framePosY, null);
    }
    
}
