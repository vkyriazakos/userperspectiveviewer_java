import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.List;

import javax.swing.AbstractButton;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import org.opencv.core.Core;
import org.openni.Device;
import org.openni.DeviceInfo;
import org.openni.ImageRegistrationMode;
import org.openni.OpenNI;
import org.openni.SensorType;
import org.openni.VideoStream;
//import org.opencv.core.Core;  

public class MainGui {
	
	private JFrame mFrame;
	private JPanel mButtonsPanel ;
	private RGBViewer mRGBViewer ;
	private DepthViewer mDepthViewer ;
	private CompositeViewer mCompositeViewer ;
	private boolean mShouldRun ;
	private Device mKinect ;
	private HistogramViewer mHistogramViewer;
	private UserPerspectiveViewer mUserPerspectiveViewer;
	private JButton startStream, stopStream,startUserPerspective ;
		
	public MainGui() {

		// Try to Load OpenNI.dll
		try {
	        System.load("E:\\Program Files\\OpenNI2\\Tools\\OpenNI2.dll");
	    }
	    catch (Exception e)
	    {
	        e.printStackTrace();
	    }
	//}
    OpenNI.initialize();
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
    
    //Check for Connected devices
    List<DeviceInfo> devicesInfo = OpenNI.enumerateDevices();
    if (devicesInfo.isEmpty()) {
        JOptionPane.showMessageDialog(null, "No device is connected", "Error", JOptionPane.ERROR_MESSAGE);
        return;
    }
    mShouldRun=true;
    mKinect = Device.open(devicesInfo.get(0).getUri());
	
	mFrame = new JFrame("Head-Coupled Display");
	
	
    //If Kinect is Connected, Setup the Viewers
    mButtonsPanel = new JPanel();
    mHistogramViewer = new HistogramViewer();
    mRGBViewer = new RGBViewer(VideoStream.create(mKinect, SensorType.COLOR));
    mDepthViewer = new DepthViewer(VideoStream.create(mKinect, SensorType.DEPTH ),mHistogramViewer);
    //Enable Depth Pixel to Color Pixel Coordinates Registration
    mKinect.setImageRegistrationMode(ImageRegistrationMode.DEPTH_TO_COLOR);
    //Create Composite Viewer also
    mCompositeViewer = new CompositeViewer(mRGBViewer,mDepthViewer);
    //Reference Composite Viewer to DepthViewer
    mDepthViewer.setCompositeViewer(mCompositeViewer);
    //Create User Perspective Viewer and reference Composite Viewer to it
    mUserPerspectiveViewer = new UserPerspectiveViewer(mCompositeViewer);
    mCompositeViewer.setUserPerspectiveViewer(mUserPerspectiveViewer);
    startStream = new JButton("Start Stream");
    stopStream = new JButton("Stop Stream");
    startUserPerspective = new JButton("Start User Perspective View");
    //Setup Buttons
    startStream.setVerticalTextPosition(AbstractButton.CENTER);
    startStream.setHorizontalTextPosition(AbstractButton.LEADING); //aka LEFT, for left-to-right locales
    startStream.setMnemonic(KeyEvent.VK_S);
    startStream.addActionListener(new ActionListener() {
    	  	
    	@Override
		public void actionPerformed(ActionEvent e) {
			//On Button Click Start Viewers
    		//mRGBViewer.videoStream.setMirroringEnabled(false);
    		//mDepthViewer.videoStream.setMirroringEnabled(false);
			mRGBViewer.videoStream.start();
			mDepthViewer.videoStream.start();
    		mFrame.setSize(2*mRGBViewer.getWidth()+20, 2*mRGBViewer.getHeight()+80);
			startStream.setEnabled(false);
			stopStream.setEnabled(true);
			startUserPerspective.setEnabled(true);
		}
	});
    stopStream.setEnabled(false);
    stopStream.setVerticalTextPosition(AbstractButton.CENTER);
    stopStream.setHorizontalTextPosition(AbstractButton.LEADING); //aka LEFT, for left-to-right locales
    stopStream.setMnemonic(KeyEvent.VK_P);
    stopStream.addActionListener(new ActionListener() {
    	
    	@Override
		public void actionPerformed(ActionEvent e) {
			//Stop Streams
			mRGBViewer.videoStream.stop();
			mDepthViewer.videoStream.stop();
			startStream.setEnabled(true);
			stopStream.setEnabled(false);
		}
	});
    startUserPerspective.setEnabled(false);
    startUserPerspective.setVerticalTextPosition(AbstractButton.CENTER);
    startUserPerspective.setHorizontalTextPosition(AbstractButton.LEADING); //aka LEFT, for left-to-right locales
    startUserPerspective.setMnemonic(KeyEvent.VK_U);
    startUserPerspective.addActionListener(new ActionListener() {
    	
    	@Override
		public void actionPerformed(ActionEvent e) {
			//On Button Click Start User Perspective View
    		mUserPerspectiveViewer.setSize(800, 600);
    		mUserPerspectiveViewer.setVisible(true);
    		startUserPerspective.setEnabled(false);
		}
	});
    //Register to keyEvents
    mFrame.addKeyListener(new KeyListener() {
        @Override
        public void keyTyped(KeyEvent arg0) {}
        
        @Override
        public void keyReleased(KeyEvent arg0) {}
        
        @Override
        public void keyPressed(KeyEvent arg0) {
            if (arg0.getKeyCode() == KeyEvent.VK_ESCAPE) {
                mShouldRun = false;
            }
        }
    });    
 // register to closing event
    mFrame.addWindowListener(new WindowAdapter() {
        @Override
        public void windowClosing(WindowEvent e) {
            mShouldRun = false;
        }
    });
    //Setup Viewers and Panel
    mFrame.setLayout(new BorderLayout());
    JPanel viewerBox = new JPanel(new GridLayout(2,2));
 
    viewerBox.add(mRGBViewer);
	viewerBox.add(mDepthViewer);
	viewerBox.add(mHistogramViewer);
	viewerBox.add(mCompositeViewer);
	
	mButtonsPanel.setLayout(new BoxLayout(mButtonsPanel, BoxLayout.LINE_AXIS));
	mButtonsPanel.add(startStream);
	mButtonsPanel.add(stopStream);
	mButtonsPanel.add(startUserPerspective);
	
	//Setup Frame
	mFrame.add(mButtonsPanel, BorderLayout.PAGE_START);
	mFrame.add(viewerBox, BorderLayout.CENTER);
	
   // mFrame.add("Center", mDepthPanel);
   // mFrame.add("East", mHistogramPanel);
    mFrame.setSize(2*mRGBViewer.getWidth()+20, 2*mRGBViewer.getHeight()+80);
    mFrame.setVisible(true);
	
	}
	
	void run() {
		
   	   while (mShouldRun) {
           try {
               Thread.sleep(200);
           } catch (InterruptedException e) {
               e.printStackTrace();
           }
       }
       mFrame.dispose();
       mUserPerspectiveViewer.dispose();
       mRGBViewer.videoStream.destroy();
       mDepthViewer.videoStream.destroy();
       
   }
	
	public static void main(String[] args) {

		final MainGui app= new MainGui();
		app.run();

	}

}
