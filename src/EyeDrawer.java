import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;

import javax.swing.JFrame;

import org.opencv.core.Point3;
public class EyeDrawer extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -7827451371429117276L;
	
	
	final public int drawersWidth=400;
	final public int drawersHeight=300;
	
	final public int monitorWidth=45;//drawersWidth/2;
	final public int monitorHeight=27;//drawersHeight/2;
	final public int monitorDepth=10;
	final public int eyeSize=10;
	
	public EyeTracker mEyeTracker;
	
	public EyeDrawer(EyeTracker eyeTracker){
		
		//JFrame Constructors
		super("Eye Position Graphic Representation");
		setLayout(new BorderLayout());
		setSize(400, 600);
		//EyeTracker
		mEyeTracker=eyeTracker;
	}
	
	
	@Override
	public synchronized void paint(Graphics g){
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		
		Point3 eye=mEyeTracker.getEye();
		//Draw First View, Front
		g2.setColor(Color.GREEN);
	    g2.setFont(new Font( "SansSerif", Font.BOLD, 12 ));
	    g2.drawString("Front View", getWidth()/2-40, 50);
	    	//Draw Screen
			g2.setColor(Color.BLACK);
			g2.fillRect(getWidth()/2-monitorWidth/2, getHeight()/4-monitorHeight/2, monitorWidth, monitorHeight);
			//Draw Eye
			g2.setColor(Color.RED);
			g2.fillOval(getWidth()/2+(int)(eye.x/10),getHeight()/4-(int)(eye.y/10),eyeSize,eyeSize);
		//Draw Separating line
		g2.drawLine(0, getHeight()/2, getWidth(), getHeight()/2);
		//Draw First View, Front
		g2.setColor(Color.GREEN);
	    g2.setFont(new Font( "SansSerif", Font.BOLD, 12 ));
	    g2.drawString("Top View", getWidth()/2-40, getHeight()/2+25);
	    	//Draw Screen
			g2.setColor(Color.BLACK);
			g2.fillRect(getWidth()/2-monitorWidth/2, 6*getHeight()/10-monitorDepth/2, monitorWidth, monitorDepth);
			//Draw Eye
			g2.setColor(Color.RED);
			g2.fillOval(getWidth()/2+(int)(eye.x/10),6*getHeight()/10+(int)(eye.z/10),eyeSize,eyeSize);
		
		//Draw Eye Pos
		g2.setColor(Color.GREEN);
	    g2.setFont(new Font( "SansSerif", Font.BOLD, 12 ));
	    g2.drawString(String.format("Eye Pos(cm): X=%.2f Y=%.2f Z=%.2f", eye.x/10.0,eye.y/10.0,eye.z/10), 25, getHeight()-25);
					
	}
	
	@Override
	public void setVisible(boolean b){
		super.setVisible(b);
		setSize(400, 600);
	}


}

