

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import org.openni.VideoMode;
import org.openni.VideoStream;
//import static java.lang.System.out;

public class RGBViewer extends AbstractViewer{
	
	private static final long serialVersionUID = 2L;
	
	
	public RGBViewer(VideoStream videostream) {
		super(videostream);
		//Setup Stream for RGB Image
    	this.videoStream.setVideoMode(new VideoMode(
				Constant.COLOR_WIDTH, 
				Constant.COLOR_HEIGHT,
				Constant.COLOR_FPS,
				Constant.COLOR_PIXEL_FORMAT));
		//this.videoStream.setMirroringEnabled(true);
		this.setSize(Constant.COLOR_WIDTH, Constant.COLOR_HEIGHT);  
		
	}		
		
	

	@Override
	public synchronized void onFrameReady(VideoStream arg0) {
		mLastFrame = videoStream.readFrame();
        ByteBuffer frameData = mLastFrame.getData().order(ByteOrder.LITTLE_ENDIAN);
        
        // make sure we have enough room
        if (mImagePixels == null || 
        				mImagePixels.length < mLastFrame.getWidth()*mLastFrame.getHeight()) {
            mImagePixels = new int[mLastFrame.getWidth()*mLastFrame.getHeight()];
        }
        //out.print("Color: "+mLastFrame.getWidth()+"x"+mLastFrame.getHeight()+"\n");
        //Take Pixel Data and put it all together by shifting to the left
        int pos = 0;
        while (frameData.remaining() > 0) {
            int red = (int)frameData.get() & 0xFF;
            int green = (int)frameData.get() & 0xFF;
            int blue = (int)frameData.get() & 0xFF;

            mImagePixels[pos] = 0xFF000000 | (red << 16) | (green << 8) | blue;
            pos++;
        }
		
        //repaint();
	}
	
}
