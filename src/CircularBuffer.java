import java.util.ArrayList;


public class CircularBuffer<T extends Number> {
	
	private ArrayList<T> mBuffer ;
	private int lastIndex;
	private Double mAverage;
	private int mSize;
	
	public CircularBuffer(int numElements) {
		mBuffer =  new ArrayList<T>();
		mSize = numElements;
		lastIndex=0;
	}
	
	public void Add(T element){
		if ( mBuffer.isEmpty()){
			for(int i=0;i<mSize;i++){
				mBuffer.add(element);
			}
		}
		else{
			mBuffer.set(lastIndex,element);
			lastIndex++;
			lastIndex%=mBuffer.size();
		}
	}
	
	public Double getAverage() {
		mAverage=mBuffer.get(0).doubleValue();
		for(int i=1;i<mBuffer.size();i++){		
			mAverage=mAverage+mBuffer.get(i).doubleValue();
		}
		return mAverage/(double)mBuffer.size();		
	}

}
