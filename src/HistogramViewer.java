//import static java.lang.System.out;

import java.awt.Component;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Color;


public class HistogramViewer extends Component {
	
	public float mHistogram[];
	private int size;
	private int ROI;
	public int maxDepth;
	public int avgDepth;
	/**
	 * 
	 */
	private static final long serialVersionUID = 3799531325476043815L;
	//Constructors
	public HistogramViewer(){
		mHistogram=new float[500];
		this.size=500;
		this.setSize(640,480);
	}
	
	public HistogramViewer(int size){
		mHistogram=new float[size];
		this.size=size;
	}
		
	//Setters-Getters
	public int getHistogramSize()
	{
		return size;
	}
	
	//Fill Method from DepthViewers Histogram	
	public void fillAndDraw(float otherHistogram[])
	{
		int index;
		int maxPoints=0;
		//Fill every 20 depth levels for HistogramDrAwing purposes
		long sum=0;
		long pointsSum=0;
		for(int i = 0;i<10000;i+=20){
			index=i/20;
			mHistogram[index]=0;
			for(int j=0;j<20;j++){
				mHistogram[index]+=otherHistogram[i+j];
				//weighted average
				sum+=(i+j)*otherHistogram[i+j];
				pointsSum+=otherHistogram[i+j];
			}
			if((int)mHistogram[index]>maxPoints) maxPoints=(int)mHistogram[index];  
		}
		//avgDepth
		avgDepth=(int) (sum/pointsSum);
		//Normalize 0-1
		for(int i=0;i<500;i++){
			mHistogram[i]/=maxPoints;
		}
		//Calculate ROI
		calcROI();
		repaint();
	}
	
	@Override
	public synchronized void paint(Graphics g){
		super.paint(g);
		Graphics2D g2 = (Graphics2D) g;
		g2.setColor(Color.BLACK);
		//First determined size of FILLED Array
		int filledIndex = getWidth();
		for(int i=getHistogramSize()-1;i>=0;i--){
			if (mHistogram[i]!=0) {
				filledIndex=i;
				maxDepth=i*20;
				break;
			}
		}
		//Then Print Histogram to fill the size of the GUI Component
	    final int BARWIDTH = Math.floorDiv(getWidth(),filledIndex);
	    final int HISTOGRAM_HEIGHT=getHeight();
	    final int X_POSITION = 0;
	    final int Y_POSITION = HISTOGRAM_HEIGHT;
	    for(int i=0;i<getHistogramSize();i++){
	      g2.fillRect(X_POSITION +i*BARWIDTH , Y_POSITION , BARWIDTH , (int)(-(float)HISTOGRAM_HEIGHT*(mHistogram[i])));
	    }	  
	    //Also Draw ROI
	    g2.setColor(Color.RED);
	    g2.fillRect(X_POSITION +(ROI/20)*BARWIDTH , Y_POSITION , BARWIDTH , (int)(-(float)HISTOGRAM_HEIGHT*1));
	    //Also Draw avgDepth and MaxDepth
	    g2.setColor(Color.GREEN);
	    g2.fillRect(X_POSITION +(avgDepth/20)*BARWIDTH , Y_POSITION , BARWIDTH , (int)(-(float)HISTOGRAM_HEIGHT*1));
	    
	    g2.setColor(Color.YELLOW);
	    g2.fillRect(X_POSITION +(maxDepth/20)*BARWIDTH , Y_POSITION , BARWIDTH , (int)(-(float)HISTOGRAM_HEIGHT*1));
	}
	
	private void calcROI(){
		ROI=10000;
		int foundZero=0;
		final float THRESHOLD=(float) 0.015;
		final int CONSECUTIVE_DEPTH_LEVELS=10;
		boolean foundNonZero=false;
        for (int i = 1; i < mHistogram.length; i++) {
        	//Check firstly for non-zero
        	//*
        	if(foundNonZero==false)
        	{
        		if(mHistogram[i]>10*THRESHOLD){
        			foundNonZero=true;
        			//out.println(20*i+" NonZero "+mHistogram[i]);
        		}
        	}
        	if(foundNonZero==true){
        		//Find 5 consecutive depth levels of 0.0
        		if(mHistogram[i]<=THRESHOLD && foundZero<CONSECUTIVE_DEPTH_LEVELS) {
        			foundZero++;
        			if(foundZero==CONSECUTIVE_DEPTH_LEVELS){
        			ROI=(i-CONSECUTIVE_DEPTH_LEVELS+1)*20;
        			//out.println(ROI+" Zero"+mHistogram[i]);
        			break;
        			}
        		//If not Consecutive appropriate depth level, zero counter
        		}else if(foundZero!=0 && mHistogram[i] >THRESHOLD){
        			foundZero=0;
        		}
        	}        
        }
        //ROI=10000;
	
		
	}
	
	public int getROI(){
		return ROI;
	}
	
}
